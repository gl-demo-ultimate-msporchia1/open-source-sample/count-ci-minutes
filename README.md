# Count Ci Minutes

This script can be used to recover the amount of seconds your CI jobs ran, given a list of project in .csv format

[[_TOC_]]

## Contributions / Support

This tool is maintained by the me and is not included in your GitLab Support if you have a license. For support questions please create an issue or ping me.

## Use Case

Use this script to evaluate sample projects when thinking about migrating from Self Managed to SaaS (GitLab.com)

## Install Method

### pip Install

Requires at least Python 3.X

Create a PAT from your GitLab instance that has access to all the projects specified in the projectlist.csv, then, if you are on a Mac:
```bash
pip install -r requirements.txt
export GITLAB_PRIVATE_TOKEN={YOURPATTOKEN}
export GITLAB_ENDPOINT={yourgitlabinstance}
```

### Local Usage

```bash
python3 index.py projectlist.csv
```

Wait for it to finish (be careful with your GitLab API limits), and the final count should be given to you via system console.