# A python script that reads each rows from a CSV file passed via argument

import csv
import os
from sys import argv
import gitlab
import datetime
from datetime import datetime, date, timedelta

gitlab_private_token = os.environ.get('GITLAB_PRIVATE_TOKEN','NA')
gitlab_endpoint = os.environ.get('GITLAB_ENDPOINT','https://gitlab.com')

print(gitlab_private_token)
print(gitlab_endpoint)


project_ids = []
with open(argv[1], 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        project_ids.append(row[0])

# print project ids
print(project_ids)

# Using python-gitlab library, run through project_ids and list all jobs


gl = gitlab.Gitlab(gitlab_endpoint, private_token=gitlab_private_token, api_version=4, per_page=5, pagination="keyset")

total_duration_given_list=0
for project_id in project_ids:
    jobs = gl.projects.get(project_id).jobs.list(iterator=True)
    total_project_duration=0
    today = date.today()  
    days30 = today - timedelta(days=30)
    print(days30)
    for job in jobs:
        if (job.duration):
            started_at_datetime = datetime.strptime(job.started_at, '%Y-%m-%dT%H:%M:%S.%fZ').date()
            if started_at_datetime > days30:
                # print(job.id, job.duration)
                total_project_duration+=job.duration
            else:
                print("No more jobs, I should stop iterating, the one I got is from ", started_at_datetime)
                break
    print(f"Total job duration for jobs in project {project_id} until {days30} is {total_project_duration} ")
    total_duration_given_list+=total_project_duration

print(f"Total job duration for jobs until {days30} in the selected list of projects is {total_duration_given_list} ")

